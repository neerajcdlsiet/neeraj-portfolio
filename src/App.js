
import './App.css';
import Home from './components/Home';
import Header from './components/Header';


import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";


function App() {
  return (
    <div>
      
      <Router>
      <Header />
        <Routes>
          <Route exact path='/' element={<Home />} />
        </Routes>
      </Router>

    </div>
  );
}

export default App;
