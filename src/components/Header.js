import React from "react";
import "./Header.css";
import logo from "../assets/img/logo.png";
import { HashLink } from "react-router-hash-link";

function Header() {
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky">
    <HashLink href="" className="logo">
      <img src={logo} />
    </HashLink>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto toTop">
        <li class="nav-item active ">
          <HashLink to={"/#home"}>Home</HashLink>
        </li>
        <li class="nav-item">
          <HashLink to={"/#about"}>About</HashLink>
        </li>
        <li>
          <HashLink to={"/#education"}>Education</HashLink>
        </li>
        <li>
          <HashLink to={"/#projects"}>Projects</HashLink>
        </li>
        <li>
          <HashLink to={"/#services"}>Services</HashLink>
        </li>
        <li>
          <HashLink to={"/#hobbies"}>Hobbies</HashLink>
        </li>
        <li>
          <HashLink to={"/#contact"}>Contact</HashLink>
        </li>
        <li>
          <HashLink to={"/#footer"}>Footer</HashLink>
        </li>
      </ul>
    </div>
  </nav>  
  );
}

export default Header;

