import React from "react";
import "./Home.css";
import { HashLink } from "react-router-hash-link";
// import FileSaver from "file-saver";

const Home = () => {
  return (
    <div className="main-container-all">
      {/* ================================================
                                    Home section
                    ================================================= */}
      <section id="home" className="home-section">
        <div className="inner-width">
          <div className="content">
            <h1>Hi I'm</h1>
            <div className="social-icon">
              <a
                href="https://www.facebook.com/kumarneeraj.sonu.9"
                className="fab fa-facebook"
                target="_blank"
              ></a>
              <a
                href="https://mobile.twitter.com/Neeraj72465479
                                "
                className="fab fa-twitter"
                target={"_blank"}
              ></a>
              <a
                href="https://www.linkedin.com/in/neeraj-k-469807180"
                className="fab fa-linkedin in"
                target={"_blank"}
              ></a>
              <a
                href="https://www.instagram.com/jj_neeso/"
                className="fab fa-instagram"
                target={"_blank"}
              ></a>
            </div>
            <div className="buttons">
              <HashLink to={"#contact"}>Contact me</HashLink>
              <a href="NeerajResume.pdf" download="NeerajResume.pdf">
                Download CV
              </a>
            </div>
          </div>
        </div>
      </section>

      {/* ================================================
                                    About section
                    ================================================= */}
      <section id="about">
        <div className="inner-width">
          <h1 className="section-title">About</h1>
          <div className="about-content">
            <img src={require("../assets/img/neeraj_pic.jpg")} alt="" />
            <div className="about-text">
              <h2>Hi! I'm Neeraj</h2>
              <h3>
                <span>Web Designer</span>
                <span>Frontend Web Developer</span>
                <span>Software Engineer</span>
              </h3>

              <p>
                My name is <strong> Neeraj kumar</strong> and I am 26 years old.
                I am from <strong>gurgaon</strong> and i currently live in{" "}
                <strong>mohali</strong>. My higher qualification is{" "}
                <strong>b.tech</strong>, which is completed in{" "}
                <strong>2019</strong> from GJU University. My stream is{" "}
                <strong>computer science</strong> and Now i am{" "}
                <strong>frontend web developer. </strong>I have total{" "}
                <strong>1.5 year experience</strong> in frontend development. 1
                year experience in <strong>web designing </strong>
                and 6 months experience in <strong>ReactJs. </strong>
                My strength is hardworking, self-motivated and flexible in any
                environment. My weakness is I trust people easily and i cannot
                say no, if someone asks for help. My long term goal is to
                achieve a good positon in tha company in that Where I Work. My
                hobbies are drawing, playing cricket and vollyball, watching tv
                and making new friends. that's all about me.
              </p>
            </div>
          </div>

          <div className="skills">
            <div className="skill">
              <div className="skill-info">
                <span>HTML</span>
                <span>70%</span>
              </div>
              <div className="skill-bar html"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>CSS</span>
                <span>75%</span>
              </div>
              <div className="skill-bar css"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>JavaScript</span>
                <span>70%</span>
              </div>
              <div className="skill-bar js"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>BOOTSTRAP</span>
                <span>60%</span>
              </div>
              <div className="skill-bar bootstrap"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>JQUERY</span>
                <span>50%</span>
              </div>
              <div className="skill-bar jquery"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>SASS</span>
                <span>50%</span>
              </div>
              <div className="skill-bar sass"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>PHOTOSHOP</span>
                <span>40%</span>
              </div>
              <div className="skill-bar photoshop"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>REACT.JS</span>
                <span>60%</span>
              </div>
              <div className="skill-bar react"></div>
            </div>

            <div className="skill">
              <div className="skill-info">
                <span>CORE JAVA</span>
                <span>30%</span>
              </div>
              <div className="skill-bar java"></div>
            </div>
          </div>
        </div>
      </section>

      {/* ================================================
                      Education section
      ================================================= */}

      <section id="education">
        <div className="inner-width">
          <h1 className="section-title">Education & Experience</h1>
          <div className="time-line">
            <div className="block">
              <h4>Nov 2021 - November 2022 </h4>
              <h3>Experience (Parangat Technologies Pvt. Ltd.)</h3>
              <p>
                Here, I worked first 6 months on web designing and last 6 months
                worked on react.js. In react.js, i worked on 2 projects, one was
                cv-form which was completed and second was attendence portal.
              </p>
            </div>

            <div className="block">
              <h4>1 July 2021 - 30 November 2021</h4>
              <h3>Experience (Dbug Lab Pvt. Ltd.)</h3>
              <p>
                Here, I worked on html,css,javascript, bootstrap and photoshop.
                And approx worked on above 10 to 12 projects in behalf of
                designing.
              </p>
            </div>
            <div className="block">
              <h4>August 2019 - February 2020 </h4>
              <h3>
                Training on frontend development from Aptron Solution(Gurgaon)
              </h3>
              <p>
                Here, I learned html, css, javascript, jquery, bootstrap and
                basic photoshop.
              </p>
            </div>
            <div className="block">
              <h4>2016 - 2019</h4>
              <h3>Education (B.Tech)</h3>
              <p>
                I have done my B.tech (gratuation) from GJU University in 2019.
                My stream is computer science.
              </p>
            </div>

            <div className="block">
              <h4>2015 - 2016 (August - February)</h4>
              <h3>Experience (SGS Tekniks Pvt. Ltd.)</h3>
              <p>
                I had worked in these company after completed my Polytechnic.
                here, I was in IGI-department as a junior engineer and my role
                was any product to identify then after checkout physically and
                error-measurement process.
              </p>
            </div>

            <div className="block">
              <h4>2013 - 2015</h4>
              <h3>Education (Polytechnic)</h3>
              <p>
                I have done my Polytechnic (Diploma) from C.R.P collage (Rohtak)
                in 2019. My stream is Electronics and Communication.
              </p>
            </div>

            <div className="block">
              <h4>2011 - 2013</h4>
              <h3>
                Education (12<sup>th</sup> Intermediate)
              </h3>
              <p>
                My Intermediate education has completed from Govt School
                Kadipur.
              </p>
            </div>

            <div className="block">
              <h4>2010 - 2011</h4>
              <h3>
                Education (10<sup>th</sup> SSC)
              </h3>
              <p>
                My Senior Secondary school education has completed from CBSE
                school
              </p>
            </div>
          </div>
        </div>
      </section>
      {/* ================================================
                                    Projects section
                    ================================================= */}
      <section id="projects">
        <div className="inner-width">
          <h1 className="section-title">Projects</h1>
          <div className="projects">
            
            <div className="card project" style={{ width: "18rem" }}>
              <img className="card-img-top" src={require("../assets/img/form_screenshot.png")} alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">Login-Register Form</h5>
                <p className="card-text">
                     Here i use registeration , Login form and Dashboard with use LocalStorage 
                </p>
                <a href="#" className="btn btn-primary">
                  Project Link Here
                </a>
              </div>
            </div>

            <div className="card project" style={{ width: "18rem" }}>
              <img className="card-img-top" src={require("../assets/img/product_screenshot.png")} alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">List Projucts of Amezon/Flipcart</h5>
                <p className="card-text">
                     Here i use registeration, Login and Dashboard 
                </p>
                <a href="https://lootersislandadvance.vercel.app/" target="_blank" className="btn btn-primary">
                  Project Link Here
                </a>
              </div>
            </div>

            {/* <div className="card project" style={{ width: "18rem" }}>
              <img className="card-img-top" src={require("../assets/img/product_screenshot.png")} alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">List Projucts of Amezon/Flipcart</h5>
                <p className="card-text">
                     Here i use registeration, Login and Dashboard 
                </p>
                <a href="#" className="btn btn-primary">
                  Project Link Here
                </a>
              </div>
            </div> */}

            {/* <div className="card project" style={{ width: "18rem" }}>
              <img className="card-img-top" src={require("../assets/img/product_screenshot.png")} alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">List Projucts of Amezon/Flipcart</h5>
                <p className="card-text">
                     Here i use registeration, Login and Dashboard 
                </p>
                <a href="#" className="btn btn-primary">
                  Project Link Here
                </a>
              </div>
            </div> */}

            {/* <div className="card project" style={{ width: "18rem" }}>
              <img className="card-img-top" src={require("../assets/img/product_screenshot.png")} alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">List Projucts of Amezon/Flipcart</h5>
                <p className="card-text">
                     Here i use registeration, Login and Dashboard 
                </p>
                <a href="#" className="btn btn-primary">
                  Project Link Here
                </a>
              </div>
            </div> */}

            {/* <div className="card project" style={{ width: "18rem" }}>
              <img className="card-img-top" src={require("../assets/img/product_screenshot.png")} alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">List Projucts of Amezon/Flipcart</h5>
                <p className="card-text">
                     Here i use registeration, Login and Dashboard 
                </p>
                <a href="#" className="btn btn-primary">
                  Project Link Here
                </a>
              </div>
            </div> */}

            
          </div>
        </div>
      </section>

      {/* ================================================
                                    Services section
                    ================================================= */}

      <section id="services">
        <div className="inner-width">
          <h1 className="section-title">Services</h1>
          <div className="servicess">
            <div className="service">
              <i className="icon fas fa-paint-brush"></i>
              <h4>Design</h4>
              <p>
                A Design is a plan or Specification for the construction of an
                object or system. I can used all sequence of activities or
                methods for any design.
              </p>
            </div>

            <div className="service">
              <i className="icon fas fa-pager"></i>
              <h4>Build Static websites</h4>
              <p>
                A static website contains Web Pages with fixed content. Each
                page is coded in HTML and displays the same information to every
                visitor.
              </p>
            </div>

            <div className="service">
              <i className="icon fas fa-credit-card"></i>
              <h4>UI desginer</h4>
              <p>
                User interface (UI) design is the process designers use to build
                interfaces in software or computerized devices, focusing on
                looks or style. Designers aim to create interfaces which users
                find easy to use and pleasurable
              </p>
            </div>

            <div className="service">
              <i className="icon fas fa-photo"></i>
              <h4>UX Designer</h4>
              <p>
                UX design is the process of designing (digital or physical)
                products that are useful, easy to use, and delightful to
                interact with. It's about enhancing the experience that people
                have while interacting with your product, and making sure they
                find value in what you're providing.
              </p>
            </div>

            <div className="service">
              <i className="icon fas fa-mobile"></i>
              <h4>Responsiveness</h4>
              <p>
                Responsive design is a user interface (UI) design approach used
                to create content that adjusts smoothly to various screen sizes.
                Designers size elements in apply media queries, so their designs
                can automatically adapt to the browser space to ensure content
                consistency across devices.
              </p>
            </div>

            <div className="service">
              <i className="icon fas fa-mobile"></i>
              <h4>Android</h4>
              <p>
                Android App is a software designed to run on an Android device
                or emulator. The term also refers to an APK file which stands
                for Android package. This file is a Zip archive containing app
                code, resources, and meta information. Android apps can be
                written in Kotlin, Java, and C++ and are run inside Virtual
                Machine.
              </p>
            </div>
          </div>
        </div>
      </section>

      {/* ================================================
                                    Hobbies section
                    ================================================= */}

      <section id="hobbies">
        <div className="inner-width">
          <h1 className="section-title">Hobbies</h1>
          <div className="hobbies">
            <a href="img/licening_song.jpg" className="hobbie">
              <img src={require("../assets/img/drawing.jpg")} alt="" />
              <div className="info">
                <h3>Listening Songs</h3>
                <div className="drawing">
                  i listen many time of song like hindi panjabi haryanvi english
                </div>
              </div>
            </a>

            <a href="img/licening_song.jpg" className="hobbie">
              <img src={require("../assets/img/licening_song.jpg")} alt="" />
              <div className="info">
                <h3>Listening Songs</h3>
                <div className="drawing">
                  i listen many time of song like hindi panjabi haryanvi english
                </div>
              </div>
            </a>

            <a href="img/playing_cricket.jpg" className="hobbie">
              <img src={require("../assets/img/playing_cricket.jpg")} alt="" />
              <div className="info">
                <h3>Playing Cricket</h3>
                <div className="drawing">
                  cricket is my favourite game i full enjoy during playing
                </div>
              </div>
            </a>

            <a href="img/playing_vollyball.jpg" className="hobbie">
              <img
                src={require("../assets/img/playing_vollyball.jpg")}
                alt=""
              />
              <div className="info">
                <h3>Playing Vollyball</h3>
                <div className="drawing">vollyball game is also enjoy</div>
              </div>
            </a>

            <a href="img/watch_cricket.jpg" className="hobbie">
              <img src={require("../assets/img/watch_cricket.jpg")} alt="" />
              <div className="info">
                <h3>Watching Cricket</h3>
                <div className="drawing">i enjoy all formats of crickets</div>
              </div>
            </a>
            <a href="img/make_new_frd.jpg" className="hobbie">
              <img src={require("../assets/img/make_new_frd.jpg")} alt="" />
              <div className="info">
                <h3>Making New Friend</h3>
                <div className="drawing">
                  I'm ready any time to making new friends
                </div>
              </div>
            </a>
          </div>
        </div>
      </section>

      {/* ================================================
                                    Contact section
                    ================================================= */}

      <section id="contact">
        <div className="inner-width">
          <h1 className="section-title">Get in Touch</h1>
          <div className="contact-info">
            <div className="item">
              <i className="fas fa-mobile-alt"></i>
              +91-7015595337
            </div>

            <div className="item">
              <i className="fas fa-envelope"></i>
              nk686421@gmail.com
            </div>

            <div className="item">
              <i className="fas fa-map-marker-alt"></i>
              Gurgaon (Haryana)
            </div>
          </div>

          <form action="#" className="contact-form">
            <input
              type="text"
              className="nameZone"
              placeholder="Your Full Name"
            />
            <input
              type="email"
              className="emailZone"
              placeholder="Your Email Id"
            />
            <input type="text" className="subjectZone" placeholder="Subject" />
            <textarea className="messageZone" />
            <input type="submit" value="send Message" className="btn" />
          </form>
        </div>
      </section>

      {/* ================================================
                                    Footer section
                    ================================================= */}
      <section id="footer">
        <div className="inner-width footers">
          <div className="copyright">
            &copy; 2020 | Created & Designed By <a href="">Neeraj kumar</a>
          </div>
          <div className="social-icon">
            <a
              href="https://www.facebook.com/kumarneeraj.sonu.9"
              className="fab fa-facebook"
            ></a>
            <a
              href="https://mobile.twitter.com/Neeraj72465479"
              className="fab fa-twitter twit"
            ></a>
            <a
              href="https://www.linkedin.com/in/neeraj-kumar-469807180"
              className="fab fa-linkedin linkedin"
            ></a>
            <a
              href="https://www.instagram.com/jj_neeso/"
              className="fab fa-instagram insta"
            ></a>
          </div>
        </div>

        <div className="goto-part">
          <button className="goTop fas fa-arrow-up"></button>
        </div>
      </section>
    </div>
  );
};

export default Home;
